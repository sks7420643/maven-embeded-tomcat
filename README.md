# Maven + Servlet + JSP + Embedded Tomcat

[eu](README.md) | [es](README.es.md) | [en](README.en.md)

Maven proiektu hau erabiliko dugu gure proiektutako oinarri moduan.

## Visual Studio Code + Java

VSCode editorea erabiliko dugu klasean.

### VSCode + Java konfiguratzen

VSCode editorean Java proiektuak exekutatu ahal izateko jarraitu pausu hauek:

* Ireki VSCode
* **Extensions (Ctrl + Shift + X)** gainean sakatu.
* Bilatu **Extension Pack for Java** eta instalatu (6 modulo instalatuko ditu: *Language Support for Java, Debugger, Java Test Runner, Maven for Java, Java Dependency Viewer and Visual Studio IntelliCode*).
* Behin instalatuta, sakatu **F1** eta idatzi **Configure Java Runtime**.
* Izan daiteke jartzea JDK instalaziorik ez dela detektatu.
  * Horrela bata, joan **install** aukerara eta sakatu **Download OpenJDK 17**.
    * ```OpenJDK17*.msi``` fitxategia deskargatuko da.
    * Instalatu defektuzko baloreekin.
    * Joan atzera ere VSCode editorera eta sakatu **Reload window** botoia.
  * Edo bilatu OpenJDK17, deskargatu eta deskonprimatu/instalatu.

### Maven instalatzen eta *path*era gehitzen

* Maven ez baduzu instalatuta:
  * Sartu zaitez [https://maven.apache.org/download.cgi](https://maven.apache.org/download.cgi) webgunean.
  * Deskargatu **Binary zip archive**.
  * Deskonprimatu nonbait (adibidez: C:\opt).
  * Widowsen, ireki **View Advanced System Settings** (*Ver la configuración avanzada del sistema* in Spanish)-
  * ```Advanced > Environment Variables > System Variables > Path > Edit > New```
  * Gehitu ```C:\opt\apache-maven-3.6.3\bin``` (edo deskonprimatu duzun karpeta) eta klikatu **Accept**.
  * Itxi VSCode lehio guztiak aldaketak aplikatzeko.

### Repositorio hau klonatu eta exekutatu

Orain beharrezkoak diren elementu guztiak instalatu ditugula, jarrai dezagun lanean:

* Sortu karpeta bat proiektu guztiak gordetzeko (adibidez: ```backend-workspace```).
* Ireki.
* Git instalatuta badaukazu:
  * ```Right-click > Git Bash here``` eta clone bat egin (Git Bash irekiko da, clone komandoa exekutatu horrela):


```console
git clone https://gitlab.com/mgep-web-ingeniaritza-1/back-end/maven-embeded-tomcat.git
```

  * Itxi Git Bash.
* Ez baduzu git instalatuta:
  * Sartu [https://gitlab.com/mgep-web-ingeniaritza-1/back-end/maven-embeded-tomcat](https://gitlab.com/mgep-web-ingeniaritza-1/back-end/maven-embeded-tomcat) webgunera.
  * Kodea deskargatu (*clone* botoiaren alboan deskargatzeko botoi bat dago).
  * Deskonprimatu lehen sortu dugun karpetan.
* Joan VSCodera, ```File > Open Folder...``` eta aukeratu sortu den karpeta **maven-embedded-tomcat**. **KONTUZ!** ez ireki workspace-a, proiektu bakoitza VSCode instantzia ezberdin batean ireki.
* ```Terminal > New Terminal``` eta exekutatu 2 komando hauek (```mvn package``` eta ```mvn install```):

```console
PS C:\Users\aperez\backend-workspace\00-maven-embedded-tomcat> mvn package
...
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  13.637 s
[INFO] Finished at: 2020-05-13T18:17:19+02:00
[INFO] ------------------------------------------------------------------------
PS C:\Users\aperez\backend-workspace\00-maven-embedded-tomcat> mvn install
...
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  13.637 s
[INFO] Finished at: 2020-05-13T18:17:19+02:00
[INFO] ------------------------------------------------------------------------
...
```

* Saguko eskuineko botoia sakatu ```src > main > java> ... >main.java``` fitxategiaren gainean eta "Run Java". Hau ikusiko duzu:

```console
...
may. 13, 2020 6:18:11 P. M. org.apache.coyote.AbstractProtocol start
INFORMACIN: Starting ProtocolHandler ["http-nio-8080"]
...
```

Edo linuxen:
```console
./target/bin/maven_embedded_tomcat_launch
```

#### Debugatzen

Oso garrantzitsua da aplikazioa debugatzen jakitzea, arazoak daudenean errazago identifikatu ahal izateko:

* Ireki ```src > main > java> ... > controller > HelloController.java``` eta gehitu breakpoint bat ```doGet``` funtzioan.
* Eskuineko klika egin ```src > main > java > launch > Main.java``` fitxategiaren gainean eta sakatu **Debug Java**.
* Ireki [http://localhost:8080/hello](http://localhost:8080/hello) nabigatzaile batean, debugerra gelditu beharko litzateke breakpoint horretan.
* Ez baduzu lortzen galdetu irakasleari.

## Erreferentziak

[create-a-java-web-application-using-embedded-tomcat](https://devcenter.heroku.com/articles/create-a-java-web-application-using-embedded-tomcat) eta
[maven-tomcat-embeded-webproject](https://github.com/richard937/maven-tomcat-embed-webproject) proiektuetan oinarrituta.

### Beste esteka interesgarri batzuk

* [Java in Visual Studio Code](https://code.visualstudio.com/docs/languages/java).
* [How to install maven in windows](https://mkyong.com/maven/how-to-install-maven-in-windows/).
* [VSCode + Java Getting started tutorial](https://code.visualstudio.com/docs/java/java-tutorial).

### Hurrengoa

* &rarr; [01-kaixo-mundua](https://gitlab.com/mgep-web-ingeniaritza-1/back-end/kaixo-mundua)