<!---------------------------->
<!-- multilingual suffix: en, es, eu -->
<!-- no suffix: eu -->
<!---------------------------->
<!-- [common] -->
# Maven + Servlet + JSP + Embedded Tomcat

[eu](README.md) | [es](README.es.md) | [en](README.en.md)

<!-- [en] -->
This is the base maven project we will used in class.

<!-- [eu] -->
Maven proiektu hau erabiliko dugu gure proiektutako oinarri moduan.

<!-- [es] -->
Usaremos este proyecto Maven como base para nuestras aplicaciones.

<!-- [common] -->
## Visual Studio Code + Java

<!-- [en] -->
We are going to be working with VSCode at class.

<!-- [eu] -->
VSCode editorea erabiliko dugu klasean.

<!-- [es] -->
Usaremos el editor VSCode en clase.

<!-- [en] -->
### Configuring VSCode + Java

To be able to ejecute Java projects in VSCode follow the next steps:

* Open VSCode
* Click on **Extensions (Ctrl + Shift + X)**.
* Search for **Extension Pack for Java** and install it (It will install 6 modules: *Language Support for Java, Debugger, Java Test Runner, Maven for Java, Java Dependency Viewer and Visual Studio IntelliCode*).
* Once installed, press **F1** and write **Configure Java Runtime**.
* Probably it will show that no *JDK installation was detected*.
  * If so, Go to **install** and click on **Download OpenJDK 17**.
    * ```OpenJDK17*.msi``` file will be downloaded.
    * Install it with default values.
    * Go back to VSCode and click on **Reload window** button.
  * Or search for **OpenJDK17** on the Internet, download it and unzip/install it.

<!-- [eu] -->
### VSCode + Java konfiguratzen

VSCode editorean Java proiektuak exekutatu ahal izateko jarraitu pausu hauek:

* Ireki VSCode
* **Extensions (Ctrl + Shift + X)** gainean sakatu.
* Bilatu **Extension Pack for Java** eta instalatu (6 modulo instalatuko ditu: *Language Support for Java, Debugger, Java Test Runner, Maven for Java, Java Dependency Viewer and Visual Studio IntelliCode*).
* Behin instalatuta, sakatu **F1** eta idatzi **Configure Java Runtime**.
* Izan daiteke jartzea JDK instalaziorik ez dela detektatu.
  * Horrela bata, joan **install** aukerara eta sakatu **Download OpenJDK 17**.
    * ```OpenJDK17*.msi``` fitxategia deskargatuko da.
    * Instalatu defektuzko baloreekin.
    * Joan atzera ere VSCode editorera eta sakatu **Reload window** botoia.
  * Edo bilatu OpenJDK17, deskargatu eta deskonprimatu/instalatu.

<!-- [es] -->
### Configurando VSCode + Java

Para poder ejecutar proyectos Java en VSCode sigue los siguientes pasos:

* Abre VSCode
* Haz clic en **Extensions (Ctrl + Shift + X)**.
* Busca **Extension Pack for Java** e instalalo (instalará 6 módulos: *Language Support for Java, Debugger, Java Test Runner, Maven for Java, Java Dependency Viewer and Visual Studio IntelliCode*).
* Una vez instalado, presiona **F1** y escribe **Configure Java Runtime**.
* Puede que te aparezca que nos e ha detectado ninguna instalacción de JDK.
  * Si es así, ve a **install** y haz clic en **Download OpenJDK 17**.
    * Se descargará ```OpenJDK17*.msi```.
    * Instalalo con los valores por defecto.
    * Vuelve a VSCode y haz clic en el botón **Reload window**.
  * O busca OpenJDK17, descargado y descomprimelo/instalalo.

<!-- [en] -->
### Installing maven and adding it to the Path

* If you don't have maven installed:
  * Visit https://maven.apache.org/download.cgi
  * Download **Binary zip archive**.
  * Unzip it somewhere (e.g.: C:\opt).
  * On Windows, open **View Advanced System Settings** (*Ver la configuración avanzada del sistema* in Spanish)-
  * ```Advanced > Environment Variables > System Variables > Path > Edit > New```
  * Add ```C:\opt\apache-maven-3.6.3\bin``` (or wherever your mvn binary has been unziped) and click on **Accept**.
  * Close any VSCode window that you have to apply all changes.

<!-- [eu] -->
### Maven instalatzen eta *path*era gehitzen

* Maven ez baduzu instalatuta:
  * Sartu zaitez [https://maven.apache.org/download.cgi](https://maven.apache.org/download.cgi) webgunean.
  * Deskargatu **Binary zip archive**.
  * Deskonprimatu nonbait (adibidez: C:\opt).
  * Widowsen, ireki **View Advanced System Settings** (*Ver la configuración avanzada del sistema* in Spanish)-
  * ```Advanced > Environment Variables > System Variables > Path > Edit > New```
  * Gehitu ```C:\opt\apache-maven-3.6.3\bin``` (edo deskonprimatu duzun karpeta) eta klikatu **Accept**.
  * Itxi VSCode lehio guztiak aldaketak aplikatzeko.

<!-- [es] -->
### Instalando Maven y añadiendolo al Path

* Si no tienes maven instalado:
  * Visita [https://maven.apache.org/download.cgi](https://maven.apache.org/download.cgi)
  * Descarga **Binary zip archive**.
  * Descomprimelo en algún sitio (e.g.: C:\opt).
  * En Windows, abre **View Advanced System Settings** (*Ver la configuración avanzada del sistema* en castellano).
  * ```Advanced > Environment Variables > System Variables > Path > Edit > New```
  * Añade ```C:\opt\apache-maven-3.6.3\bin``` (o la carpeta en la que hayas descomprimido **el binario** de mvn) y haz clicen **Accept**.
  * Cierra cualquier ventana de VSCode que tengas abierta para aplicar los cambios.

<!-- [en] -->
### Clone this repo and execute it

Now that we have all the needed elements installed, let's continue and start working:

* Create a folder called ```backend-workspace``` wherever you want.
* Open it.
* If you have git installed:
  * ```Right-click > Git Bash here``` and make a clone (It will open the forder using Git Bash, you should write the clone command as follows):

<!-- [eu] -->
### Repositorio hau klonatu eta exekutatu

Orain beharrezkoak diren elementu guztiak instalatu ditugula, jarrai dezagun lanean:

* Sortu karpeta bat proiektu guztiak gordetzeko (adibidez: ```backend-workspace```).
* Ireki.
* Git instalatuta badaukazu:
  * ```Right-click > Git Bash here``` eta clone bat egin (Git Bash irekiko da, clone komandoa exekutatu horrela):

<!-- [es] -->
### Clona este repositorio y ejecutalo

Ahora que tenemos todos los elementos instalados, empezemos a trabajar:

* Crea una carpeta para los proyectos de clase (por ejemplo: ```backend-workspace```).
* Abrela.
* Si tienes Git instalado:
  * ```Right-click > Git Bash here``` y haz un clone (abrirá Git Bash y podrás hacer un clone de esta manera):
<!-- [common] -->

```console
git clone https://gitlab.com/mgep-web-ingeniaritza-1/back-end/maven-embeded-tomcat.git
```

<!-- [en] -->
  * Close Git Bash.
* If you don't have git installed:
  * Visit [https://gitlab.com/mgep-web-ingeniaritza-1/back-end/maven-embeded-tomcat](https://gitlab.com/mgep-web-ingeniaritza-1/back-end/maven-embeded-tomcat)
  * Download the code (there is a button next to "clone" to download the code).
  * Unzip it on your workspace.
* Go to VSCode and ```File > Open Folder...``` and select the folder **maven-embedded-tomcat**. **WARNING** do not open backend-workspace, open each application in a VSCode instance.
* ```Terminal > New Terminal``` and execute this 2 commands (```mvn package``` and ```mvn install```):
<!-- [eu] -->
  * Itxi Git Bash.
* Ez baduzu git instalatuta:
  * Sartu [https://gitlab.com/mgep-web-ingeniaritza-1/back-end/maven-embeded-tomcat](https://gitlab.com/mgep-web-ingeniaritza-1/back-end/maven-embeded-tomcat) webgunera.
  * Kodea deskargatu (*clone* botoiaren alboan deskargatzeko botoi bat dago).
  * Deskonprimatu lehen sortu dugun karpetan.
* Joan VSCodera, ```File > Open Folder...``` eta aukeratu sortu den karpeta **maven-embedded-tomcat**. **KONTUZ!** ez ireki workspace-a, proiektu bakoitza VSCode instantzia ezberdin batean ireki.
* ```Terminal > New Terminal``` eta exekutatu 2 komando hauek (```mvn package``` eta ```mvn install```):
<!-- [es] -->
  * Cierra Git Bash.
* Si no tienes git instalado:
  * Entra en [https://gitlab.com/mgep-web-ingeniaritza-1/back-end/maven-embeded-tomcat](https://gitlab.com/mgep-web-ingeniaritza-1/back-end/maven-embeded-tomcat)
  * Descarga el código (hay un botón al lado de *clone* para hacerlo).
  * Descomprimelo en tu workspace.
* Vete a VSCode y ```File > Open Folder...``` y selecciona la carpeta **maven-embedded-tomcat**. **CUIDADO!** No abras el workspace, abre cada aplicación de VSCode in una instancia diferente.
* ```Terminal > New Terminal``` y ejecuta estos 2 comandos (```mvn package``` y ```mvn install```):
<!-- [common] -->

```console
PS C:\Users\aperez\backend-workspace\00-maven-embedded-tomcat> mvn package
...
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  13.637 s
[INFO] Finished at: 2020-05-13T18:17:19+02:00
[INFO] ------------------------------------------------------------------------
PS C:\Users\aperez\backend-workspace\00-maven-embedded-tomcat> mvn install
...
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  13.637 s
[INFO] Finished at: 2020-05-13T18:17:19+02:00
[INFO] ------------------------------------------------------------------------
...
```

<!-- [en] -->
* Right click on ```src > main > java> ... >main.java``` and click "Run Java". You will see the folowing:
<!-- [eu] -->
* Saguko eskuineko botoia sakatu ```src > main > java> ... >main.java``` fitxategiaren gainean eta "Run Java". Hau ikusiko duzu:
<!-- [es] -->
* Click derecho en ```src > main > java> ... >main.java``` y click sobre "Run Java". Verás lo siguiente:
<!-- [common] -->

```console
...
may. 13, 2020 6:18:11 P. M. org.apache.coyote.AbstractProtocol start
INFORMACIN: Starting ProtocolHandler ["http-nio-8080"]
...
```

<!-- [en] -->
* Opening [http://localhost:8080](http://localhost:8080) on a browser you should see "Hello WebEng1! [JSP]" and clicking on the link you should see "Hello WebEng1! [Servlet]".

You have compiled your first Java Web Application using Maven and embedded Tomcat!
```Ctrl + C``` to stop the server.

* You can also run the application, being at 00-maven-embedded-tomcat folder executing the binary:
<!-- [eu] -->
* Nabigatzailean [http://localhost:8080](http://localhost:8080) irekitzen baduzu "Hello WebEng1! [JSP]" testua ikusi beharko zenuke eta azpiko estekan sakatu ezkero "Hello WebEng1! [Servlet]".

Zure lehenengo Java Web Aplikazioa konpilatu duzu Maven eta Tomcat erabiliz!
Sakatu ```Ctrl + C``` zerbitzaria martxan dagoen terminalean zerbitzaria gelditzeko.

* Aplikazioa binarioa exekutatuz martxan jarri dezakezu honela:
<!-- [es] -->
* Si abres [http://localhost:8080](http://localhost:8080) en un navegador verás el texto "Hello WebEng1! [JSP]" y si haces click en el enlace de abajo verás el texto "Hello WebEng1! [Servlet]".

¡Acabas de compilar tu primera Aplicación Web Java usando Maven y Tomcat!
```Ctrl + C``` para cerrar el servidor.

* También puedes ejecutar la aplicación mediante el binario de esta forma:

<!-- [common] -->
```console
.\target\bin\maven_embedded_tomcat_launch.bat
```

<!-- [en] -->
Or in linux:
<!-- [eu] -->
Edo linuxen:
<!-- [es] -->
O en linux:

<!-- [common] -->
```console
./target/bin/maven_embedded_tomcat_launch
```

<!-- [en] -->
#### Debugging

It is very important to be able to debug the application so we can see what it is happening in the insides:

* Open ```src > main > java> ... > controller > HelloController.java``` and add a breakpoint inside ```doGet``` function.
* Right click on ```src > main > java > launch > Main.java``` and **Debug Java**.
* Open http://localhost:8080/hello in the browser, debuger should have stopped in the breakpoint.
* If it does not work, tell the teacher.

<!-- [eu] -->
#### Debugatzen

Oso garrantzitsua da aplikazioa debugatzen jakitzea, arazoak daudenean errazago identifikatu ahal izateko:

* Ireki ```src > main > java> ... > controller > HelloController.java``` eta gehitu breakpoint bat ```doGet``` funtzioan.
* Eskuineko klika egin ```src > main > java > launch > Main.java``` fitxategiaren gainean eta sakatu **Debug Java**.
* Ireki [http://localhost:8080/hello](http://localhost:8080/hello) nabigatzaile batean, debugerra gelditu beharko litzateke breakpoint horretan.
* Ez baduzu lortzen galdetu irakasleari.

<!-- [es] -->
#### Depurando

Es muy importante poder depurar la aplicación, para poder identificar qué problema tenemos más fácilmente:

* Abre ```src > main > java> ... > controller > HelloController.java``` y añade un breakpoint dentro de la función ```doGet```.
* Clic derecho sobre ```src > main > java > launch > Main.java``` y clica en **Debug Java**.
* Abre [http://localhost:8080/hello](http://localhost:8080/hello) en un navegador, el debuger debería haber parado en el breakpoint.
* Si no lo has podido poner en marcha, hablá con el profesor.

<!-- [en] -->
## References

Based on [create-a-java-web-application-using-embedded-tomcat](https://devcenter.heroku.com/articles/create-a-java-web-application-using-embedded-tomcat)
and
[maven-tomcat-embeded-webproject](https://github.com/richard937/maven-tomcat-embed-webproject)

<!-- [eu] -->
## Erreferentziak

[create-a-java-web-application-using-embedded-tomcat](https://devcenter.heroku.com/articles/create-a-java-web-application-using-embedded-tomcat) eta
[maven-tomcat-embeded-webproject](https://github.com/richard937/maven-tomcat-embed-webproject) proiektuetan oinarrituta.

<!-- [es] -->
## Referencias

Basado en [create-a-java-web-application-using-embedded-tomcat](https://devcenter.heroku.com/articles/create-a-java-web-application-using-embedded-tomcat)
y
[maven-tomcat-embeded-webproject](https://github.com/richard937/maven-tomcat-embed-webproject)

<!-- [en] -->
### Other Interesting Links

<!-- [eu] -->
### Beste esteka interesgarri batzuk

<!-- [es] -->
### Otros enlaces interesantes

<!-- [common] -->
* [Java in Visual Studio Code](https://code.visualstudio.com/docs/languages/java).
* [How to install maven in windows](https://mkyong.com/maven/how-to-install-maven-in-windows/).
* [VSCode + Java Getting started tutorial](https://code.visualstudio.com/docs/java/java-tutorial).

<!-- [en] -->
### Next

<!-- [eu] -->
### Hurrengoa

<!-- [es] -->
### Siguiente

<!-- [common] -->
* &rarr; [01-kaixo-mundua](https://gitlab.com/mgep-web-ingeniaritza-1/back-end/kaixo-mundua)
