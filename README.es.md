# Maven + Servlet + JSP + Embedded Tomcat

[eu](README.md) | [es](README.es.md) | [en](README.en.md)

Usaremos este proyecto Maven como base para nuestras aplicaciones.

## Visual Studio Code + Java

Usaremos el editor VSCode en clase.

### Configurando VSCode + Java

Para poder ejecutar proyectos Java en VSCode sigue los siguientes pasos:

* Abre VSCode
* Haz clic en **Extensions (Ctrl + Shift + X)**.
* Busca **Extension Pack for Java** e instalalo (instalará 6 módulos: *Language Support for Java, Debugger, Java Test Runner, Maven for Java, Java Dependency Viewer and Visual Studio IntelliCode*).
* Una vez instalado, presiona **F1** y escribe **Configure Java Runtime**.
* Puede que te aparezca que nos e ha detectado ninguna instalacción de JDK.
  * Si es así, ve a **install** y haz clic en **Download OpenJDK 17**.
    * Se descargará ```OpenJDK17*.msi```.
    * Instalalo con los valores por defecto.
    * Vuelve a VSCode y haz clic en el botón **Reload window**.
  * O busca OpenJDK17, descargado y descomprimelo/instalalo.

### Instalando Maven y añadiendolo al Path

* Si no tienes maven instalado:
  * Visita [https://maven.apache.org/download.cgi](https://maven.apache.org/download.cgi)
  * Descarga **Binary zip archive**.
  * Descomprimelo en algún sitio (e.g.: C:\opt).
  * En Windows, abre **View Advanced System Settings** (*Ver la configuración avanzada del sistema* en castellano).
  * ```Advanced > Environment Variables > System Variables > Path > Edit > New```
  * Añade ```C:\opt\apache-maven-3.6.3\bin``` (o la carpeta en la que hayas descomprimido **el binario** de mvn) y haz clicen **Accept**.
  * Cierra cualquier ventana de VSCode que tengas abierta para aplicar los cambios.

### Clona este repositorio y ejecutalo

Ahora que tenemos todos los elementos instalados, empezemos a trabajar:

* Crea una carpeta para los proyectos de clase (por ejemplo: ```backend-workspace```).
* Abrela.
* Si tienes Git instalado:
  * ```Right-click > Git Bash here``` y haz un clone (abrirá Git Bash y podrás hacer un clone de esta manera):

```console
git clone https://gitlab.com/mgep-web-ingeniaritza-1/back-end/maven-embeded-tomcat.git
```

  * Cierra Git Bash.
* Si no tienes git instalado:
  * Entra en [https://gitlab.com/mgep-web-ingeniaritza-1/back-end/maven-embeded-tomcat](https://gitlab.com/mgep-web-ingeniaritza-1/back-end/maven-embeded-tomcat)
  * Descarga el código (hay un botón al lado de *clone* para hacerlo).
  * Descomprimelo en tu workspace.
* Vete a VSCode y ```File > Open Folder...``` y selecciona la carpeta **maven-embedded-tomcat**. **CUIDADO!** No abras el workspace, abre cada aplicación de VSCode in una instancia diferente.
* ```Terminal > New Terminal``` y ejecuta estos 2 comandos (```mvn package``` y ```mvn install```):

```console
PS C:\Users\aperez\backend-workspace\00-maven-embedded-tomcat> mvn package
...
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  13.637 s
[INFO] Finished at: 2020-05-13T18:17:19+02:00
[INFO] ------------------------------------------------------------------------
PS C:\Users\aperez\backend-workspace\00-maven-embedded-tomcat> mvn install
...
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  13.637 s
[INFO] Finished at: 2020-05-13T18:17:19+02:00
[INFO] ------------------------------------------------------------------------
...
```

* Click derecho en ```src > main > java> ... >main.java``` y click sobre "Run Java". Verás lo siguiente:

```console
...
may. 13, 2020 6:18:11 P. M. org.apache.coyote.AbstractProtocol start
INFORMACIN: Starting ProtocolHandler ["http-nio-8080"]
...
```

O en linux:

```console
./target/bin/maven_embedded_tomcat_launch
```

#### Depurando

Es muy importante poder depurar la aplicación, para poder identificar qué problema tenemos más fácilmente:

* Abre ```src > main > java> ... > controller > HelloController.java``` y añade un breakpoint dentro de la función ```doGet```.
* Clic derecho sobre ```src > main > java > launch > Main.java``` y clica en **Debug Java**.
* Abre [http://localhost:8080/hello](http://localhost:8080/hello) en un navegador, el debuger debería haber parado en el breakpoint.
* Si no lo has podido poner en marcha, hablá con el profesor.

## Referencias

Basado en [create-a-java-web-application-using-embedded-tomcat](https://devcenter.heroku.com/articles/create-a-java-web-application-using-embedded-tomcat)
y
[maven-tomcat-embeded-webproject](https://github.com/richard937/maven-tomcat-embed-webproject)

### Otros enlaces interesantes

* [Java in Visual Studio Code](https://code.visualstudio.com/docs/languages/java).
* [How to install maven in windows](https://mkyong.com/maven/how-to-install-maven-in-windows/).
* [VSCode + Java Getting started tutorial](https://code.visualstudio.com/docs/java/java-tutorial).

### Siguiente

* &rarr; [01-kaixo-mundua](https://gitlab.com/mgep-web-ingeniaritza-1/back-end/kaixo-mundua)