package edu.mondragon.webeng1.maven_embedded_tomcat_launch.controller;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(name = "MyServlet", urlPatterns = { "/hello" })
public class HelloController extends HttpServlet {
    private static final long serialVersionUID = -2612653958349060415L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletOutputStream out = resp.getOutputStream();
        out.write("<h1>Hello WebEng1! [Servlet]</h1><p><a href='/'> Go to JSP</a>.</p>".getBytes());
        out.flush();
        out.close();
    }
}