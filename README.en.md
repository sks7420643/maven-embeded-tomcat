# Maven + Servlet + JSP + Embedded Tomcat

[eu](README.md) | [es](README.es.md) | [en](README.en.md)

This is the base maven project we will used in class.

## Visual Studio Code + Java

We are going to be working with VSCode at class.

### Configuring VSCode + Java

To be able to ejecute Java projects in VSCode follow the next steps:

* Open VSCode
* Click on **Extensions (Ctrl + Shift + X)**.
* Search for **Extension Pack for Java** and install it (It will install 6 modules: *Language Support for Java, Debugger, Java Test Runner, Maven for Java, Java Dependency Viewer and Visual Studio IntelliCode*).
* Once installed, press **F1** and write **Configure Java Runtime**.
* Probably it will show that no *JDK installation was detected*.
  * If so, Go to **install** and click on **Download OpenJDK 17**.
    * ```OpenJDK17*.msi``` file will be downloaded.
    * Install it with default values.
    * Go back to VSCode and click on **Reload window** button.
  * Or search for **OpenJDK17** on the Internet, download it and unzip/install it.

### Installing maven and adding it to the Path

* If you don't have maven installed:
  * Visit https://maven.apache.org/download.cgi
  * Download **Binary zip archive**.
  * Unzip it somewhere (e.g.: C:\opt).
  * On Windows, open **View Advanced System Settings** (*Ver la configuración avanzada del sistema* in Spanish)-
  * ```Advanced > Environment Variables > System Variables > Path > Edit > New```
  * Add ```C:\opt\apache-maven-3.6.3\bin``` (or wherever your mvn binary has been unziped) and click on **Accept**.
  * Close any VSCode window that you have to apply all changes.

### Clone this repo and execute it

Now that we have all the needed elements installed, let's continue and start working:

* Create a folder called ```backend-workspace``` wherever you want.
* Open it.
* If you have git installed:
  * ```Right-click > Git Bash here``` and make a clone (It will open the forder using Git Bash, you should write the clone command as follows):


```console
git clone https://gitlab.com/mgep-web-ingeniaritza-1/back-end/maven-embeded-tomcat.git
```

  * Close Git Bash.
* If you don't have git installed:
  * Visit [https://gitlab.com/mgep-web-ingeniaritza-1/back-end/maven-embeded-tomcat](https://gitlab.com/mgep-web-ingeniaritza-1/back-end/maven-embeded-tomcat)
  * Download the code (there is a button next to "clone" to download the code).
  * Unzip it on your workspace.
* Go to VSCode and ```File > Open Folder...``` and select the folder **maven-embedded-tomcat**. **WARNING** do not open backend-workspace, open each application in a VSCode instance.
* ```Terminal > New Terminal``` and execute this 2 commands (```mvn package``` and ```mvn install```):

```console
PS C:\Users\aperez\backend-workspace\00-maven-embedded-tomcat> mvn package
...
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  13.637 s
[INFO] Finished at: 2020-05-13T18:17:19+02:00
[INFO] ------------------------------------------------------------------------
PS C:\Users\aperez\backend-workspace\00-maven-embedded-tomcat> mvn install
...
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  13.637 s
[INFO] Finished at: 2020-05-13T18:17:19+02:00
[INFO] ------------------------------------------------------------------------
...
```

* Right click on ```src > main > java> ... >main.java``` and click "Run Java". You will see the folowing:

```console
...
may. 13, 2020 6:18:11 P. M. org.apache.coyote.AbstractProtocol start
INFORMACIN: Starting ProtocolHandler ["http-nio-8080"]
...
```

* Opening [http://localhost:8080](http://localhost:8080) on a browser you should see "Hello WebEng1! [JSP]" and clicking on the link you should see "Hello WebEng1! [Servlet]".

You have compiled your first Java Web Application using Maven and embedded Tomcat!
```Ctrl + C``` to stop the server.

* You can also run the application, being at 00-maven-embedded-tomcat folder executing the binary:
<!-- [eu] -->
* Nabigatzailean [http://localhost:8080](http://localhost:8080) irekitzen baduzu "Hello WebEng1! [JSP]" testua ikusi beharko zenuke eta azpiko estekan sakatu ezkero "Hello WebEng1! [Servlet]".

Zure lehenengo Java Web Aplikazioa konpilatu duzu Maven eta Tomcat erabiliz!
Sakatu ```Ctrl + C``` zerbitzaria martxan dagoen terminalean zerbitzaria gelditzeko.

* Aplikazioa binarioa exekutatuz martxan jarri dezakezu honela:
<!-- [es] -->
* Si abres [http://localhost:8080](http://localhost:8080) en un navegador verás el texto "Hello WebEng1! [JSP]" y si haces click en el enlace de abajo verás el texto "Hello WebEng1! [Servlet]".

¡Acabas de compilar tu primera Aplicación Web Java usando Maven y Tomcat!
```Ctrl + C``` para cerrar el servidor.

* También puedes ejecutar la aplicación mediante el binario de esta forma:

<!-- [common] -->
```console
.\target\bin\maven_embedded_tomcat_launch.bat
```

Or in linux:
```console
./target/bin/maven_embedded_tomcat_launch
```

#### Debugging

It is very important to be able to debug the application so we can see what it is happening in the insides:

* Open ```src > main > java> ... > controller > HelloController.java``` and add a breakpoint inside ```doGet``` function.
* Right click on ```src > main > java > launch > Main.java``` and **Debug Java**.
* Open http://localhost:8080/hello in the browser, debuger should have stopped in the breakpoint.
* If it does not work, tell the teacher.

## References

Based on [create-a-java-web-application-using-embedded-tomcat](https://devcenter.heroku.com/articles/create-a-java-web-application-using-embedded-tomcat)
and
[maven-tomcat-embeded-webproject](https://github.com/richard937/maven-tomcat-embed-webproject)

### Other Interesting Links

* [Java in Visual Studio Code](https://code.visualstudio.com/docs/languages/java).
* [How to install maven in windows](https://mkyong.com/maven/how-to-install-maven-in-windows/).
* [VSCode + Java Getting started tutorial](https://code.visualstudio.com/docs/java/java-tutorial).

### Next

* &rarr; [01-kaixo-mundua](https://gitlab.com/mgep-web-ingeniaritza-1/back-end/kaixo-mundua)